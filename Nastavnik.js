const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Nastavnik = sequelize.define("nastavnik",{
        ime:Sequelize.STRING,
        prezime:Sequelize.STRING,
        titula:Sequelize.STRING
    })
    return Nastavnik;
}