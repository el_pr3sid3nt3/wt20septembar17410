const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Lekcija = sequelize.define("lekcija",{
        naziv:Sequelize.STRING,
        brojCasova:Sequelize.INTEGER
    })
    return Lekcija;
}