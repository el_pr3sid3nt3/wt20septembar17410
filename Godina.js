const Sequelize = require("sequelize")

module.exports = function(sequelize,DataTypes){
    const Godina = sequelize.define("godina",{
        naziv:Sequelize.STRING
    })
    return Godina;
}