const Sequelize = require("sequelize");
const sequelize = new Sequelize("wtispit","root","",{
    host:'127.0.0.1',
    dialect:"mysql",
    logging:false,
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.kreirajBazu = function(){
    var con = mysql.createConnection({
        host:"127.0.0.1",
        user:"root",
        password:""
    });
    con.connect(function(err){
        if(err) throw err;
        console.log("Povezan");
        con.query("CREATE DATABASE wtispit",function(err,result){
            if(err)throw err;
            console.log("Baza kreirana");
        })
    })
}
db.godina = sequelize.import(__dirname+'/Godina.js');
db.lekcija = sequelize.import(__dirname+'/Lekcija.js');
db.nastavnik = sequelize.import(__dirname+'/Nastavnik.js');
db.predmet = sequelize.import(__dirname+'/Predmet.js');


/* lekcije i predmeti */
db.predmet.hasMany(db.lekcija,{as:"lekcije"});
db.nastavnik.hasMany(db.predmet,{as:"nastavnik"});
//db.lekcija.belongsTo(db.predmet,{as:"lekcija"});

/* predmet i nastavnik */
//db.predmet.belongsTo(db.nastavnik,{as:"predmet"});

/* foreignkey za koji je predmet taj predmet preduslov */
db.predmet.hasOne(db.predmet,{as:"preduslov"});

/* jedna godina ima više predmeta ali jedan predmet pripada samo jednoj godini */
db.godina.hasMany(db.predmet,{as:"predmeti"});



module.exports = db;