
const db = require(__dirname + '/db.js')
const mysql = require('mysql2');

const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

db.sequelize.sync({ force: true }).then(function () {
    console.log("Gotovo kreiranje tabela");

})

app.post('/lekcija',function(req,res){
    var tijelo = req.body;
    var predmetId = tijelo["predmet"];
    var naziv = tijelo["naziv"];
    var brojCasova = tijelo["brojCasova"];

    db.predmet.findOne({where:{id:predmetId}}).then(function(dobijeniPredmet){
        db.lekcija.findAll({where:{predmetId:predmetId}}).then(function(dobijeneLekcije){
            var brojCasovaTrenutnihLekcija = 0;
            for(i=0;i<dobijeneLekcije.length;i++){
                brojCasovaTrenutnihLekcija += dobijeneLekcije[i].brojCasova;
            }
            if(brojCasovaTrenutnihLekcija + brojCasova > dobijeniPredmet.brojCasova){
                res.json({error:"Broj casova veci od dozvoljenog na predmetu"});
            }
            else{
                db.lekcija.create({naziv:naziv,predmetId:predmetId,brojCasova:brojCasova});
                console.log("Uspjesno dodana lekcija");
            }
        })
    })
    res.end();
})

app.get('/nastavnik/:id/brojcasova',function(req,res){
    var tijelo = req.body;
    var nastavnik = tijelo["id"];
    var ukupanBrojCasova = 0;
    db.predmet.findAll({where:{nastavnikId:nastavnik}}).then(function(predmetiNastavnika){
        
        for(i=0;i<predmetiNastavnika;i++){
            ukupanBrojCasova += predmetiNastavnika[i].brojCasova;
        }
    })
    res.send(ukupanBrojCasova);
    res.end();

})

app.get('/predmet/lekcija/',function(req,res){
    var tijelo = req.body;
    var nazivLekcije = "Neki naziv"//tijelo['naziv'];



    db.lekcija.findAll({where:{naziv:nazivLekcije}}).then(function(lekcije){
        var predmeti = [];
        for(i=0;i<lekcije.length;i++){
            db.predmet.findOne({where:{id:lekcije[i].predmetId}}).then(function(predmet){
                predmeti.push(predmet);
            })
        }
        res.send(predmeti);
    })
    res.end();
})

app.listen(8080,()=>{
    console.log("App radi na portu 8080");
})